import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-Search';
  query = '';
  result = [{ name: '', link: '' }];

  Heros = [
    { name: 'DrowRanger', link: 'https://cdn.cloudflare.steamstatic.com/apps/dota2/videos/dota_react/heroes/renders/drow_ranger.png' },
    { name: 'EmberSpirit', link: 'https://cdn.cloudflare.steamstatic.com/apps/dota2/videos/dota_react/heroes/renders/ember_spirit.png' },
    { name: 'WindRanger', link: 'https://cdn.cloudflare.steamstatic.com/apps/dota2/videos/dota_react/heroes/renders/windrunner.png' },
    { name: 'StormSpirit', link: 'https://cdn.cloudflare.steamstatic.com/apps/dota2/videos/dota_react/heroes/renders/storm_spirit.png' },
  ];


  search() {
    this.result = [{ name: '', link: '' }];
    for (let i = 0; i < this.Heros.length; i++) {
      if (this.query == this.Heros[i].name) {
        console.log(this.query);
        this.result.push(this.Heros[i]);
      }
    }
    if (this.query == 'all') {
      this.result = this.Heros;
    }
  }
}
